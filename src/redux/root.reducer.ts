import {combineReducers} from 'redux';
import auth from '../auth/auth.reducer';
import dashboard from '../dashboard/dashboard.reducer';
import {connectRouter} from 'connected-react-router';

export default history => combineReducers({
    router: connectRouter(history),
    auth,
    dashboard
});
