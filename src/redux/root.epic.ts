import {combineEpics} from 'redux-observable';
import {attemptLoginEpic, loginSuccessEpic, logoutAttemptEpic} from '../auth/auth.epic';
import {dashboardLoadStartEpic} from '../dashboard/dashboard.epic';

export const rootEpic = combineEpics(
    attemptLoginEpic,
    loginSuccessEpic,
    logoutAttemptEpic,
    dashboardLoadStartEpic,
);
