import React, {Component} from 'react';
import {attemptLoadDashboard} from './dashboard.actions';
import {connect} from 'react-redux';
import {logoutAttempt} from '../auth/auth.actions';

const mapStateToProps = (state, ownProps) => ({
    ...state
});

const mapDispatchToProps = {
    loadDashboardData: attemptLoadDashboard,
    attemptLogout: logoutAttempt
};

const connectToStore = connect(
    mapStateToProps,
    mapDispatchToProps
);

class Dashboard extends Component {
    logout = () =>{
        this.props.attemptLogout();
    }
    render() {
        let dashboardDataDivs = this.props.dashboard.dashboardData ?
            this.props.dashboard.dashboardData.map(({first_name, last_name}, key, ok) => {
                return (<div key={key}>{first_name} {last_name} </div>);
            }) :
            [<div key={'1'}>Loading...</div>];
        return (
            <>
                <div>Users:</div>
                {dashboardDataDivs}

                <button onClick={this.logout}>Logout</button>
            </>
        );
    }

    componentDidMount() {
        this.props.loadDashboardData('yep');
    }
}

export default connectToStore(Dashboard);
