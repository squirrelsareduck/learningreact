import {DASHBOARD_LOAD_ATTEMPTED, DASHBOARD_LOAD_SUCCESSFUL, DASHBOARD_LOAD_FAILED} from './dashboard.actions';
import {LOGOUT_ATTEMPTED} from '../auth/auth.actions';

const  initialState = {
    dashboardData: undefined
};

export default function(state = initialState, action) {
    console.log('in dash reducer', action);
    switch (action.type) {
        case DASHBOARD_LOAD_SUCCESSFUL:
            return {
                ...state,
                dashboardData: action.payload.data
            };
        case DASHBOARD_LOAD_ATTEMPTED:
            return {
                ...state
            };
        case DASHBOARD_LOAD_FAILED:
            return {
                ...state
            };
        case LOGOUT_ATTEMPTED:
            return {
                ...initialState
            };
        default:
            return state;
    }
};
