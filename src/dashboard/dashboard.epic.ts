import {ofType} from 'redux-observable';
import {ignoreElements, map, switchMap, tap} from 'rxjs/operators';
import {DASHBOARD_LOAD_ATTEMPTED, dashboardLoadSuccess} from './dashboard.actions';
import {from} from 'rxjs';

export const dashboardLoadStartEpic = (action$, state$) => action$.pipe(
    ofType(DASHBOARD_LOAD_ATTEMPTED),
    tap(console.log),
    switchMap(action => from(fetch('https://reqres.in/api/users?page=2', {
        method: 'GET',
        mode: 'cors',
        headers: {'Content-Type': 'application/json'}
    }).then((response) => {
        if(response.ok) return response.json();
        return ignoreElements();
    }))),
    map(dashboardLoadSuccess)
);

