import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import {Provider} from 'react-redux';
import store, {history} from './redux/store';
import {Route} from 'react-router-dom';
import Dashboard from './dashboard/Dashboard';
import {ConnectedRouter} from 'connected-react-router';

ReactDOM.render(
    <Provider store={store()}>
        <ConnectedRouter history={history}>
            <div>
                <Route path="/dashboard" component={Dashboard} />
                <Route path="/" component={App} exact />
            </div>
        </ConnectedRouter>
    </Provider>,
    document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
