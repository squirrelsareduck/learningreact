import {LOGIN_ATTEMPTED, LOGIN_SUCCESSFUL, loginFailure, loginSuccess, LOGOUT_ATTEMPTED} from './auth.actions';
import {ofType} from 'redux-observable';
import {from} from 'rxjs';
import {catchError, ignoreElements, map, mergeMap} from 'rxjs/operators';
import {push} from 'connected-react-router';

export const loginSuccessEpic = (action$, state$) => action$.pipe(
    ofType(LOGIN_SUCCESSFUL),
    map(() => push('/dashboard'))
);

export const attemptLoginEpic = (action$, state$) => action$.pipe(
    ofType(LOGIN_ATTEMPTED),
    mergeMap(action => from(
        fetch('https://reqres.in/api/login', {
            method: 'POST',
            mode: 'cors',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({"email": action.payload.username, "password": action.payload.password})
        }).then(response => {
            if(response.ok) return response.json();
            return ignoreElements();
        })
    )),
    map(loginSuccess),
    catchError(err => {
        return loginFailure(err);
    })
);

export const logoutAttemptEpic = (action$, state$) => action$.pipe(
    ofType(LOGOUT_ATTEMPTED),
    map(() => push('/'))
);
