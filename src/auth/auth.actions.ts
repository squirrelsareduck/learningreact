export const LOGIN_ATTEMPTED = 'LOGIN_ATTEMPTED';
export const LOGIN_SUCCESSFUL = 'LOGIN_SUCCESSFUL';
export const LOGIN_FAILED = 'LOGIN_FAILURE';
export const LOGOUT_ATTEMPTED = 'LOGOUT_ATTEMPTED';
export const LOGOUT_FAILED = 'LOGOUT_FAILED';
export const LOGOUT_SUCCESSFUL = 'LOGOUT_SUCCESSFUL';

export const attemptLogin = payload => {
    return {
        type: LOGIN_ATTEMPTED,
        payload
    }
};

export const loginFailure = () => {
    console.log('inside loginFailure action creator');

    return ({
        type: LOGIN_FAILED,
        payload: {}
    });
};

export const loginSuccess = payload => {
    return ({
        type: LOGIN_SUCCESSFUL,
        payload
    });
};

export const logoutAttempt = payload => {
    return ({
        type: LOGOUT_ATTEMPTED,
        payload
    })
}
