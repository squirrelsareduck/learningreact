import {LOGIN_ATTEMPTED, LOGIN_FAILED, LOGIN_SUCCESSFUL,
LOGOUT_ATTEMPTED, LOGOUT_FAILED, LOGOUT_SUCCESSFUL} from './auth.actions';

const  initialState = {
    username: undefined,
    isLoggedIn: false,
    token: undefined
};

export default function(state = initialState, action) {
    switch (action.type) {
        case LOGIN_SUCCESSFUL:
            return {
                ...state,
                isLoggedIn: true,
                token: action.payload.token
            };
        case LOGIN_FAILED:
            return {
                ...initialState
            };
        case LOGIN_ATTEMPTED:
            return {
                username: action.payload.username
            };
        case LOGOUT_ATTEMPTED:
            return {
                ...initialState
            };
        default:
            return state;
    }
}
